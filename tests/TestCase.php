<?php

namespace Tests;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Nuwave\Lighthouse\Testing\MakesGraphQLRequests;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;
    use MakesGraphQLRequests;
    use RefreshDatabase;

    public function testAuthProfileBefore(): void
    {


        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                  profile {
                    id
                    name
                  }
            }
        ');

        $response->assertJson([
            'data' => [
                'profile' => null,
            ],
        ]);
    }
    public function testAuthRegisterLogin(): void
    {

        // Регистрация
        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation ($name: String!, $email: String!, $password: String) {
                register(name: $name, email: $email, password: $password) {
                    name
                }
            }
        ', [
            'name' => 'test',
            'email' => 'test@test.ru',
            'password' => 'testtest'
        ]);

        $this->assertDatabaseHas('users', [
            'email' => 'test@test.ru',
        ]);


        $response->assertJson([
            'data' => [
                'register' => [
                    'name' => 'test',
                ],
            ],
        ]);

        // Логин
        $response = $this->graphQL(/** @lang GraphQL */ '
            mutation ($email: String!, $password: String!) {
                login(email: $email, password: $password) {
                    name
                }
            }
        ', [
            'email' => 'test@test.ru',
            'password' => 'testtest'
        ]);

        $response->assertJson([
            'data' => [
                'login' => [
                    'name' => 'test'
                ],
            ],
        ]);

        // Информация профиля
        $response = $this->graphQL(/** @lang GraphQL */ '
            {
                  profile {
                    name
                  }
            }
        ');

        $response->assertJson([
            'data' => [
                'profile' => [
                    'name' => 'test'
                ],
            ],
        ]);
    }
}
