<?php

namespace App\GraphQL\Mutations;

use App\Models\User;
use GraphQL\Error\Error;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

final class Register
{
    /**
     * @param null $_
     * @param array{} $args
     * @return User
     * @throws Error
     */
    public function __invoke($_, array $args): User
    {
        $input = collect($args)->except('password_confirmation')->toArray();
        $input['password'] = Hash::make($input['password']);

        $user = User::create($input);

        return $user;
    }
}
